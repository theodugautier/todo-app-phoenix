defmodule HelloWorld.Repo.Migrations.CreateTodos do
  use Ecto.Migration

  def change do
    create table(:todos) do
      add :name, :string
      add :content, :text
      add :is_validated, :boolean

      timestamps()
    end
  end
end
