defmodule HelloWorld.TodoLists.Todo do
  use Ecto.Schema
  import Ecto.Changeset

  schema "todos" do
    field :name, :string
    field :content, :string
    field :is_validated, :boolean

    timestamps()
  end

  @doc false
  def changeset(todo, attrs) do
    todo
    |> cast(attrs, [:name, :content, :is_validated])
    |> validate_required([:name])
  end
end
