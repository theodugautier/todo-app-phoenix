defmodule HelloWorld.TodoLists do
  @moduledoc """
  The todolist context
  """

  import Ecto.Query, warn: false
  alias HelloWorld.TodoLists.Todo
  alias HelloWorld.Repo

  @doc """
  Returns the list of todo

  ### Examples

    iex> list_todos()
    [%Todo{}, ...]
  """
  def list_todos do
    Repo.all(Todo)
  end

  @doc """
  Create Todo

  ### Examples

    iex> change_todo(%{field: value})
    {:ok, %Product{}}

    iex> change_todo(%{field: bas_value})
    {:error, %Ecto.Changeset{}}

  """
  def create_todo(attrs \\ %{}) do
    %Todo{}
    |> Todo.changeset(attrs)
    |> Repo.insert()
  end

  @doc false
  def change_todo(%Todo{} = todo) do
    Todo.changeset(todo, %{})
  end
end
