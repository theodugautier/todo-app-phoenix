defmodule HelloWorldWeb.TodoController do
  alias HelloWorld.TodoLists
  alias HelloWorld.TodoLists.Todo

  use HelloWorldWeb, :controller

  def index(conn, _params) do
    todos = TodoLists.list_todos()
    changeset = TodoLists.change_todo(%Todo{})
    render(conn, "index.html", todos: todos, changeset: changeset)
  end

  def create(conn, %{"todo" => todo_params}) do
    case TodoLists.create_todo(todo_params) do
      {:ok, _todo} ->
        conn
        |> put_flash(:info, "La tache a ete cree")
        |> redirect(to: Routes.todo_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        todos = TodoLists.list_todos()
        conn
        |> put_flash(:error, "Une erreur a ete detecte")
        |> render("index.html", todos: todos, changeset: changeset)
    end
  end
end
