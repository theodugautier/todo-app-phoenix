defmodule HelloWorld.TodoListsTest do
  use HelloWorld.DataCase

  alias HelloWorld.TodoLists

  describe "todolist" do
    alias HelloWorld.TodoLists.Todo

    @valid_attrs %{name: "good name"}
    # @update_attrs %{name: "new name"}
    @invalid_attrs %{name: nil}

    def todo_fixture(attrs \\ %{}) do
      {:ok, todo} =
        attrs
        |> Enum.into(@valid_attrs)
        |> TodoLists.create_todo()

      todo
    end

    test "list_todos returns all todos" do
      todo = todo_fixture()
      assert TodoLists.list_todos() == [todo]
    end

    test "create_todo create todos when attributes are valid" do
      assert {:ok, %Todo{} = todo} = TodoLists.create_todo(@valid_attrs)
      assert todo.name == "good name"
    end

    test "create_todo returns error when attributes are invalid" do
      assert {:error, %Ecto.Changeset{} = changeset} = TodoLists.create_todo(@invalid_attrs)
    end
  end
end
