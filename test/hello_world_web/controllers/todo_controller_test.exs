defmodule HelloWorldWeb.TodoControllerTest do
  use HelloWorldWeb.ConnCase

  alias HelloWorld.TodoList.Todo

  @create_attrs %{name: "Testing"}
  @invalid_attrs %{name: nil}

  test "GET /todo", %{conn: conn} do
    conn = get(conn, "/todo")
    assert html_response(conn, 200) =~ "Liste des taches"
  end

  describe "create todos" do
    test "redirect to index if data is valid", %{conn: conn} do
      conn = post(conn, Routes.todo_path(conn, :create), todo: @create_attrs)

      assert redirected_to(conn) == Routes.todo_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.todo_path(conn, :create), todo: @invalid_attrs)

      assert html_response(conn, 200) =~ "Une erreur a ete detecte"
    end
  end
end
